#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Есть словарь кодов товаров

goods = {
    'Лампа': '12345',
    'Стол': '23456',
    'Диван': '34567',
    'Стул': '45678',
}

# Есть словарь списков количества товаров на складе.

store = {
    '12345': [
        {'quantity': 27, 'price': 42},
    ],
    '23456': [
        {'quantity': 22, 'price': 510},
        {'quantity': 32, 'price': 520},
    ],
    '34567': [
        {'quantity': 2, 'price': 1200},
        {'quantity': 1, 'price': 1150},
    ],
    '45678': [
        {'quantity': 50, 'price': 100},
        {'quantity': 12, 'price': 95},
        {'quantity': 43, 'price': 97},
    ],
}

# Рассчитать на какую сумму лежит каждого товара на складе
# например для ламп

#lamps_cost = store[goods['Лампа']][0]['quantity'] * store[goods['Лампа']][0]['price']
# или проще (/сложнее ?)
#lamp_code = goods['Лампа']
#lamps_item = store[lamp_code][0]
#lamps_quantity = lamps_item['quantity']
#lamps_price = lamps_item['price']
#lamps_cost = lamps_quantity * lamps_price
#print('Лампа -', lamps_quantity, 'шт, стоимость', lamps_cost, 'руб')

# Вывести стоимость каждого товара на складе: один раз распечать сколько всего столов, стульев и т.д. на складе
# Формат строки <товар> - <кол-во> шт, стоимость <общая стоимость> руб

# WARNING для знающих циклы: БЕЗ циклов. Да, с переменными; да, неэффективно; да, копипаста.
# Это задание на ручное вычисление - что бы потом понять как работают циклы и насколько с ними проще жить.

lamps = goods['Лампа']
tables = goods['Стол']
sofas =  goods['Диван']
chair = goods['Стул']

sum_lam = store[lamps][0]['quantity'] * store[lamps][0]['price']
qa_lam = store[lamps][0]['quantity']
print('There is', qa_lam, 'lamps in the warehouse. It price is', sum_lam)

sum_tab = store[tables][0]['quantity'] * store[tables][0]['price'] \
          + store[tables][1]['quantity'] * store[tables][1]['price']
qa_tab = store[tables][0]['quantity'] + store[tables][1]['quantity']
print('There is', qa_tab, 'tables in the warehouse. It price is', sum_tab)

sum_sof = store[sofas][0]['quantity'] * store[sofas][0]['price'] \
          + store[sofas][1]['quantity'] * store[sofas][1]['price']
qa_sof = store[sofas][0]['quantity'] + store[sofas][1]['quantity']
print('There is', qa_sof, 'sofas in the warehouse. It price is', sum_sof)

sum_cha = store[chair][0]['quantity'] * store[chair][0]['price'] \
          + store[chair][1]['quantity'] * store[chair][1]['price'] \
          + store[chair][2]['quantity'] * store[chair][2]['price']
qa_cha = store[chair][0]['quantity'] + store[chair][1]['quantity'] + store[chair][2]['quantity']
print('There is', qa_cha, 'chairs in the warehouse. It price is', sum_cha)


##########################################################################################
# ВНИМАНИЕ! После того как __ВСЯ__ домашняя работа сделана и запушена на сервер,         #
# нужно зайти в ЛМС (LMS - Learning Management System ) по адресу http://go.skillbox.ru  #
# и оформить попытку сдачи ДЗ! Без этого ДЗ не будет проверяться!                        #
# Как оформить попытку сдачи смотрите видео - https://youtu.be/qVpN0L-C3LU               #
##########################################################################################






