#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Создайте списки:

# моя семья (минимум 3 элемента, есть еще дедушки и бабушки, если что)
my_family = ['daddy', 'mommy', 'me']


# список списков приблизителного роста членов вашей семьи
my_family_height = [
    ['daddy', 180],
    ['mommy', 170],
    ['me', 178]
]

# Выведите на консоль рост отца в формате
#   Рост отца - ХХ см
print('Height of my',my_family[0], 'is', my_family_height[0][1],'cm')

# Выведите на консоль общий рост вашей семьи как сумму ростов всех членов
#   Общий рост моей семьи - ХХ см
daddy_height = my_family_height[0][1]
mommy_height = my_family_height[1][1]
my_height = my_family_height[2][1]

total_height = daddy_height + mommy_height + my_height

print('Total height of my family is', total_height,'cm')
