# -*- coding: utf-8 -*-

import simple_draw as sd


# Часть 1.
# Написать функции рисования равносторонних геометрических фигур:
# - треугольника
# - квадрата
# - пятиугольника
# - шестиугольника
# Все функции должны принимать 3 параметра:
# - точка начала рисования
# - угол наклона
# - длина стороны
#
# Использование копи-пасты - обязательно! Даже тем кто уже знает про её пагубность. Для тренировки.
# Как работает копипаста:
#   - одну функцию написали,
#   - копипастим её, меняем название, чуть подправляем код,
#   - копипастим её, меняем название, чуть подправляем код,
#   - и так далее.
# В итоге должен получиться ПОЧТИ одинаковый код в каждой функции

# Пригодятся функции
# sd.get_point()
# sd.get_vector()
# sd.line()
# Результат решения см lesson_004/results/exercise_01_shapes.jpg

# def triangle(start_point, angle, lenght):
#     sd.vector(start_point, angle, lenght)
#     print(start_point, type(start_point))
#     print(sd.get_vector(start_point, 90, 100))
#     b_point = sd.get_vector(start_point, angle, lenght)
#     print(b_point, type(b_point))


def multiangle(start_point, angle, length):
    for i in range(sides):
        n_side = sd.get_vector(start_point, angle=angle, length=length)
        end_point = n_side.end_point
        print(start_point, end_point)
        sd.line(start_point, end_point, color=rgb[i])
        start_point = end_point
        angle = angles[sides][i-1]


angles = {
    3 : [60, -60, -180],
    4 : [90, 180, 0, -90],
    5 : [108, 36, -36, -108, 180],
    6 : [120, 60, 0, -60, -120, -180]
}
rgb = [sd.COLOR_RED, sd.COLOR_GREEN, sd.COLOR_BLUE, sd.COLOR_CYAN, sd.COLOR_YELLOW, sd.COLOR_ORANGE]
x = int(input('Input x coord 0 to 600 '))
y = int(input('Input y coord 0 to 600 '))
sides = int(input('Num of sides: 3 to 6'))
if (sides > 2) and (sides < 7):
    if sides == 3:
        start_point = sd.get_point(x, y)
        delta = (180 * (sides - 2)) / sides
        lenght = 200
        multiangle(start_point, delta, lenght)
    elif sides == 4:
        start_point = sd.get_point(x, y)
        delta = (180 * (sides - 2)) / sides
        lenght = 200
        multiangle(start_point, delta, lenght)
    elif sides == 5:
        start_point = sd.get_point(x, y)
        delta = (180 * (sides - 2)) / sides
        lenght = 200
        multiangle(start_point, delta, lenght)
    elif sides == 6:
        start_point = sd.get_point(x, y)
        delta = (180 * (sides - 2)) / sides
        lenght = 200
        multiangle(start_point, delta, lenght)
else:
    print('The num Should be in 3 to 6')

# Часть 1-бис.
# Попробуйте прикинуть обьем работы, если нужно будет внести изменения в этот код.
# Скажем, связывать точки не линиями, а дугами. Или двойными линиями. Или рисовать круги в угловых точках. Или...
# А если таких функций не 4, а 44?

# Часть 2 (делается после зачета первой части)
#
# Надо сформировать функцию, параметризированную в местах где была "небольшая правка".
# Это называется "Выделить общую часть алгоритма в отдельную функцию"
# Потом надо изменить функции рисования конкретных фигур - вызывать общую функцию вместо "почти" одинакового кода.
#
# В итоге должно получиться:
#   - одна общая функция со множеством параметров,
#   - все функции отрисовки треугольника/квадрата/етс берут 3 параметра и внутри себя ВЫЗЫВАЮТ общую функцию.
#
# Не забудте в этой общей функции придумать, как устранить разрыв
#   в начальной/конечной точках рисуемой фигуры (если он есть)

# Часть 2-бис.
# А теперь - сколько надо работы что бы внести изменения в код? Выгода на лицо :)
# Поэтому среди программистов есть принцип D.R.Y. https://clck.ru/GEsA9
# Будьте ленивыми, не используйте копи-пасту!


sd.pause()
