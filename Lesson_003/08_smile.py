# -*- coding: utf-8 -*-

# (определение функций)
import simple_draw as sd
import random as rand


# Написать функцию отрисовки смайлика в произвольной точке экрана
# Форма рожицы-смайлика на ваше усмотрение
# Параметры функции: кордината X, координата Y, цвет.
# Вывести 10 смайликов в произвольных точках экрана.

def smile(x, y, rad):
    center_point = sd.get_point(x, y)
    sd.circle(center_point, 50)
    end_point = sd.get_point(x, y + 30)
    sd.line(center_point, end_point)
    end_point = sd.get_point(x, y - 10)
    sd.line(center_point, end_point)
    start_mouth = sd.get_point(x - 30, y - 30)
    end_mouth = sd.get_point(x + 30, y - 30)
    sd.line(start_mouth, end_mouth)
    eye1_point = sd.get_point(x + 20, y + 20)
    sd.circle(eye1_point, 5)
    eye2_point = sd.get_point(x - 20, y + 20)
    sd.circle(eye2_point, 5)

for i in range(1, 11):
    i += 1
    smile(rand.randint(0, 600), rand.randint(0, 600), 30)

sd.pause()
