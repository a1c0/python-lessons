# -*- coding: utf-8 -*-

# (цикл for)
import simple_draw as sd

# Нарисовать стену из кирпичей. Размер кирпича - 100х50
# Использовать вложенные циклы for


for y in range (0, 601, 50):
    point_start = sd.get_point(0, y)
    point_end = sd.get_point(600, y)
    sd.line(point_start, point_end, color=sd.COLOR_YELLOW, width=2)
    a = (int(y % 100))
    print(a)
    if (y == 0) or a != 0:
        for x in range (0, 601, 100):
            x += 0
            point_start = sd.get_point(x, 0)
            point_end = sd.get_point(x, 600)
            sd.line(point_start, point_end, color=sd.COLOR_YELLOW, width=2)
    else:
        for x in range(0, 601, 100):
            x += 50
            point_start = sd.get_point(x, 0)
            point_end = sd.get_point(x, 600)
            sd.line(point_start, point_end, color=sd.COLOR_YELLOW, width=2)




sd.pause()
