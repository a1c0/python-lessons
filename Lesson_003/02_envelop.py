# -*- coding: utf-8 -*-

# (if/elif/else)

# Заданы размеры envelop_x, envelop_y - размеры конверта и paper_x, paper_y листа бумаги
#
# Определить, поместится ли бумага в конверте (стороны листа параллельны сторонам конверта)
#
# Результат проверки вывести на консоль (ДА/НЕТ)
# Использовать только операторы if/elif/else, можно вложенные

envelop_x, envelop_y = 10, 7

# проверить для
paper = [
    [9, 8],
    [6, 8],
    [8, 6],
    [3, 4],
    [11, 9],
    [9, 11]
]
# (просто раскоментировать нужную строку и проверить свой код)

for i, [x, y] in enumerate(paper):
    paper_x, paper_y = x, y

    if (paper_x <= envelop_x) and (paper_y <= envelop_y):
        print('В конверт с размерами', envelop_x, 'и', envelop_y, 'см бумага размерами', paper_x, 'и', paper_y,
              'см поместится')
    elif (paper_x <= envelop_y) and (paper_y <= envelop_x):
        print('В конверт с размерами', envelop_x, 'и', envelop_y, 'см бумага размерами', paper_x, 'и', paper_y,
              'см поместится')
    else:
        print('В конверт с размерами', envelop_x, 'и', envelop_y, 'см бумага размерами', paper_x, 'и', paper_y,
              'см не поместится')

# Усложненное задание, решать по желанию.
# Заданы размеры hole_x, hole_y прямоугольного отверстия и размеры brick_х, brick_у, brick_z кирпича (все размеры
# могут быть в диапазоне от 1 до 1000)
#
# Определить, пройдет ли кирпич через отверстие (грани кирпича параллельны сторонам отверстия)

hole_x, hole_y = 8, 9
brick = [
    # brick_x, brick_y, brick_z =
    [11, 10, 2],
    # brick_x, brick_y, brick_z =
    [11, 2, 10],
    # brick_x, brick_y, brick_z =
    [10, 11, 2],
    # brick_x, brick_y, brick_z =
    [10, 2, 11],
    # brick_x, brick_y, brick_z =
    [2, 10, 11],
    # brick_x, brick_y, brick_z =
    [2, 11, 10],
    # brick_x, brick_y, brick_z =
    [3, 5, 6],
    # brick_x, brick_y, brick_z =
    [3, 6, 5],
    # brick_x, brick_y, brick_z =
    [6, 3, 5],
    # brick_x, brick_y, brick_z =
    [6, 5, 3],
    # brick_x, brick_y, brick_z =
    [5, 6, 3],
    # brick_x, brick_y, brick_z =
    [5, 3, 6],
    # brick_x, brick_y, brick_z =
    [11, 3, 6],
    # brick_x, brick_y, brick_z =
    [11, 6, 3],
    # brick_x, brick_y, brick_z =
    [6, 11, 3],
    # brick_x, brick_y, brick_z =
    [6, 3, 11],
    # brick_x, brick_y, brick_z =
    [3, 6, 11],
    # brick_x, brick_y, brick_z = \
    [3, 11, 6]
]
# (просто раскоментировать нужную строку и проверить свой код)

for _, [x, y, z] in enumerate(brick):
    brick_x, brick_y, brick_z = x, y, z
    if (brick_x <= hole_x) and (brick_y <= hole_y) and ():
        print('В отверстие с размерами', hole_x, 'и', hole_y, 'см кирпич размерами', brick_x, ',', brick_y, 'и',
              brick_z, 'см поместится')
    elif (brick_x <= hole_y) and (brick_y <= hole_x):
        print('В отверстие с размерами', hole_x, 'и', hole_y, 'см кирпич размерами', brick_x, ',', brick_y, 'и',
              brick_z, 'см поместится')
    elif (brick_y <= hole_x) and (brick_z <= hole_y):
        print('В отверстие с размерами', hole_x, 'и', hole_y, 'см кирпич размерами', brick_x, ',', brick_y, 'и',
              brick_z, 'см поместится')
    elif (brick_y <= hole_y) and (brick_z <= hole_x):
        print('В отверстие с размерами', hole_x, 'и', hole_y, 'см кирпич размерами', brick_x, ',', brick_y, 'и',
              brick_z, 'см поместится')
    elif (brick_x <= hole_x) and (brick_z <= hole_y):
        print('В отверстие с размерами', hole_x, 'и', hole_y, 'см кирпич размерами', brick_x, ',', brick_y, 'и',
              brick_z, 'см поместится')
    elif (brick_x <= hole_y) and (brick_z <= hole_x):
        print('В отверстие с размерами', hole_x, 'и', hole_y, 'см кирпич размерами', brick_x, ',', brick_y, 'и',
              brick_z, 'см поместится')
    else:
        print('В отверстие с размерами', hole_x, 'и', hole_y, 'см кирпич размерами', brick_x, ',', brick_y, 'и',
          brick_z, 'см не поместится')
