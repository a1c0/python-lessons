# -*- coding: utf-8 -*-

# (if/elif/else)

# По номеру месяца вывести кол-во дней в нем (без указания названия месяца, в феврале 28 дней)
# Результат проверки вывести на консоль
# Если номер месяца некорректен - сообщить об этом

# Номер месяца получать от пользователя следующим образом
# months = {
#     'январь': 31,
#     'февраль': 28,
#     'март': 31,
#     'апрель': 30,
#     'май': 31,
#     'июнь': 30,
#     'июль': 31,
#     'август': 31,
#     'сентябрь': 30,
#     'октябрь': 31,
#     'ноябрь': 30,
#     'декабрь': 31
# }
#
num_months = {
    1: 'январь',
    2: 'февраль',
    3: 'март',
    4: 'апрель',
    5: 'май',
    6: 'июнь',
    7: 'июль',
    8: 'август',
    9: 'сентябрь',
    10: 'октябрь',
    11: 'ноябрь',
    12: 'декабрь'
}
user_input = input("Введите, пожалуйста, номер месяца: ")
month = int(user_input)
print('Вы ввели', month)


def mon_30():
    print('Это - ', num_months[month])
    print('В месяце 30 дней')


def mon_31():
    print('Это - ', num_months[month])
    print('В месяце 31 день')


# if month in range(1, 12):
#     print('Это месяц', num_months[month])
#     print('В нем', months[num_months[month]], 'дней!')

if isinstance(month, int):
    if month in range(1, 13):
        if month == 1:
            mon_31()
        elif month == 2:
            print('В месяце 28 дней')
        elif month == 3:
            mon_31()
        elif month == 4:
            mon_30()
        elif month == 5:
            mon_31()
        elif month == 6:
            mon_30()
        elif month == 7:
            mon_31()
        elif month == 8:
            mon_31()
        elif month == 9:
            mon_30()
        elif month == 10:
            mon_31()
        elif month == 11:
            mon_30()
        elif month == 12:
            mon_31()
    else:
        print('Такого месяца не существует!')
else:
    print('Введите номер месяца')


