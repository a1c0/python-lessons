# -*- coding: utf-8 -*-

# (цикл for)

import simple_draw as sd

rainbow_colors = (sd.COLOR_RED, sd.COLOR_ORANGE, sd.COLOR_YELLOW, sd.COLOR_GREEN,
                  sd.COLOR_CYAN, sd.COLOR_BLUE, sd.COLOR_PURPLE)

# Нарисовать радугу: 7 линий разного цвета толщиной 4 с шагом 5 из точки (50, 50) в точку (350, 450)
color = 0
for x in range(7):
    point_start = sd.get_point(50 + x * 5, 50)
    point_end = sd.get_point(350 + x * 5, 450)
    sd.line(point_start, point_end, rainbow_colors[color], width=5)
    color += 1


# Усложненное задание, делать по желанию.
# Нарисовать радугу дугами от окружности (cсм sd.circle) за нижним краем экрана,
# поэкспериментировать с параметрами, что бы было красиво
sd.clear_screen()
color = 0
for x in range(7):
    point = sd.get_point(300, -250 + x * 50)
    sd.circle(point, 500, rainbow_colors[color], width=50)
    color += 1

sd.pause()
