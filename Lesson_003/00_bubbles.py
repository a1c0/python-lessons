# -*- coding: utf-8 -*-

import simple_draw as sd
import random

sd.resolution = (1200, 600)

colour = {
    'red' : sd.COLOR_RED,
    'green' : sd.COLOR_GREEN,
    'blue' : sd.COLOR_BLUE,
    'black' : sd.COLOR_BLACK,
    'orange' : sd.COLOR_ORANGE,
    'purple' : sd.COLOR_DARK_PURPLE
}

# Нарисовать пузырек - три вложенных окружностей с шагом 5 пикселей
x = 100
y = 300
point = sd.get_point(x, y)
radius = 50
for _ in range(3):
    radius += 5
    sd.circle(point, radius, width=3)


# Написать функцию рисования пузырька, принммающую 2 (или более) параметра: точка рисовании и шаг
def bubble(point, step, colour):
    radius = 50
    for _ in range(3):
        radius += step
        sd.circle(point, radius, width=2, color=colour)


point = sd.get_point(100, 100)
bubble(point, 10, 'red')

# Нарисовать 10 пузырьков в ряд
for _ in range(100, 1001, 100):
    point = sd.get_point(100 + _, 500)
    bubble(point, 5, 'green')

# Нарисовать три ряда по 10 пузырьков
for i in range(0, 300, 100):
    for k in range(0, 901, 100):
        point = sd.get_point(200 + k, 100 + i)
        bubble(point, 5, 'blue')

# Нарисовать 100 пузырьков в произвольных местах экрана случайными цветами
color_list = ['red', 'green', 'blue', 'black', 'orange', 'purple']
sd.clear_screen()
for _ in range(100):
    point = sd.random_point()
    step = random.randint(1, 10)
    color = random.choice(color_list)
    print(color)
    bubble(point, step, color)

sd.pause()
