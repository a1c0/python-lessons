# -*- coding: utf-8 -*-

# (цикл while)

# Ежемесячная стипендия студента составляет educational_grant руб., а расходы на проживание превышают стипендию
# и составляют expenses руб. в месяц. Рост цен ежемесячно увеличивает расходы на 3%, кроме первого месяца
# Составьте программу расчета суммы денег, которую необходимо единовременно попросить у родителей,
# чтобы можно было прожить учебный год (10 месяцев), используя только эти деньги и стипендию.
# Формат вывода:
#   Студенту надо попросить ХХХ.ХХ рублей

educational_grant, expenses = 10000, 12000
sum_expenses = 12000
sum_grant = 10000
month = 1
print('В', month, '-й месяц затраты на проживание составят', expenses, 'рублей')
while month < 10:
    sum_expenses += expenses
    expenses = round((expenses * 3 / 100), 2) + expenses
    sum_grant += educational_grant
    month += 1
    print('В', month, '-й месяц затраты на проживание составят', round(expenses, 2), 'рублей')
print('Всего за', month,'месяцев затраты на проживание составят', round(sum_expenses, 2), 'рублей')
print('Всего получено стипендии -', round(sum_grant, 2), 'рублей')
print('Студенту надо попросить', round(sum_expenses - sum_grant,2), 'рублей')
